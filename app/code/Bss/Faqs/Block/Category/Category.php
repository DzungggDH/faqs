<?php

namespace Bss\Faqs\Block\Category;

use Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory as FaqCategoryCollectionFactory;

class Category extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Bss\Faqs\Model\ResourceModel\FaqCategory\Collection
     */
    protected $faqCategoryCollection;

    /**
     * @var \Bss\Faqs\Helper\Config
     */
    protected $configHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCategoryCollectionFactory $faqCategoryCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCategoryCollectionFactory $faqCategoryCollectionFactory,
        \Bss\Faqs\Helper\Config $configHelper
    ) {
        parent::__construct($context);
        $this->faqCategoryCollection = $faqCategoryCollectionFactory->create();
        $this->configHelper = $configHelper;
    }

    /**
     * Get a list of FAQ Categories
     *
     * @return array|null
     */
    public function getActiveFaqCategories()
    {
        $this->faqCategoryCollection->addFieldToFilter('main_table.is_active', true);
        return $this->faqCategoryCollection->getData();
    }

    /**
     * Get URL of the files in pub/media folder
     *
     * @param $path
     * @return string
     */
    public function getFileBaseUrl($path)
    {
        return $this->configHelper->getFileBaseUrl($path);
    }
}
