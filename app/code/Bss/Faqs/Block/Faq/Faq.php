<?php

namespace Bss\Faqs\Block\Faq;

use Bss\Faqs\Model\FaqCategoryFactory;
use Bss\Faqs\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;

class Faq extends \Magento\Framework\View\Element\Template
{
    /**
     * @var FaqCollectionFactory
     */
    protected $faqCollectionFactory;

    /**
     * @var \Bss\Faqs\Model\FaqCategoryFactory
     */
    protected $faqCategoryFactory;

    /**
     * @var \Bss\Faqs\Helper\Config
     */
    protected $configHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCollectionFactory $faqCollectionFactory
     * @param FaqCategoryFactory $faqCategoryFactory
     * @param \Bss\Faqs\Helper\Config $configHelper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCollectionFactory $faqCollectionFactory,
        FaqCategoryFactory $faqCategoryFactory,
        \Bss\Faqs\Helper\Config $configHelper
    ) {
        parent::__construct($context);
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->configHelper = $configHelper;
    }

    /**
     * Get FAQ Categories
     *
     * @return array|null
     */
    public function getFaqCategory()
    {
        $faqCategory = $this->faqCategoryFactory->create()->load($this->getData('category_id'))->getOrigData();
        $faqCategory['icon'] = $this->configHelper->getFileBaseUrl($faqCategory['icon']);
        return $faqCategory;
    }

    /**
     * Get a list of FAQs
     *
     * @return array|null
     */
    public function getActiveFaqs()
    {
        $faqCollection = $this->faqCollectionFactory->create();
        $faqCollection->addFieldToFilter('main_table.category_id', $this->getData('category_id'))
                    ->addFieldToFilter('main_table.is_active', true);
        return $faqCollection->getData();
    }
}
