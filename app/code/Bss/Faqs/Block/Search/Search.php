<?php

namespace Bss\Faqs\Block\Search;

use Bss\Faqs\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;

class Search extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Bss\Faqs\Model\ResourceModel\Faq\Collection
     */
    protected $faqCollection;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param FaqCollectionFactory $faqCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        FaqCollectionFactory $faqCollectionFactory
    ) {
        parent::__construct($context);
        $this->faqCollection = $faqCollectionFactory->create();
    }

    /**
     * Get Text search from url
     *
     * @return string
     */
    public function getTextSearch()
    {
        return ($this->getRequest()->getParam('s')) ? $this->escapeHtml($this->getRequest()->getParam('s')) : '';
    }

    /**
     * Get FAQs via text search
     *
     * @return array|bool
     */
    public function getFaqs()
    {
        $faqCollection = $this->faqCollection->addFieldToFilter('main_table.is_active', true);
        $textSearch = $this->getTextSearch();
        $faqCollection->addFieldToFilter('main_table.title', ['like' => '%' . $textSearch . '%']);
        return $faqCollection->getData();
    }

//    protected function clearTextSearch()
//    {
//        $textSearch = strtolower($this->getTextSearch());
//
//        while (stristr($textSearch, '-')) {
//            $textSearch = str_replace('-', ' ', $textSearch);
//        }
//
//        while (stristr($textSearch, '  ')) {
//            $textSearch = str_replace('  ', ' ', $textSearch);
//        }
//
//        $filter = new \Zend\I18n\Filter\Alnum(true);
//
//        return $filter->filter($textSearch);
//    }
}
