<?php

namespace Bss\Faqs\Model\Config;

class Category implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @var \Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory
     */
    protected $collectionFactory;

    protected $options;

    public function __construct(
        \Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        if ($this->options === null) {
            $collection = $this->collectionFactory->create();

            $this->options = [['label' => '', 'value' => '']];

            foreach ($collection as $category) {
                $this->options[] = [
                    'label' => __('%1', $category['title']),
                    'value' => $category->getId()
                ];
            }
        }

        return $this->options;
    }
}
