<?php

namespace Bss\Faqs\Model;

use Magento\Framework\Model\AbstractModel;

class FaqCategory extends AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Bss\Faqs\Model\ResourceModel\FaqCategory::class);
    }
}
