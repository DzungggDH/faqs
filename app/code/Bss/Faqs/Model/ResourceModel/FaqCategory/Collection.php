<?php

namespace Bss\Faqs\Model\ResourceModel\FaqCategory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Bss\Faqs\Model\FaqCategory::class,
            \Bss\Faqs\Model\ResourceModel\FaqCategory::class
        );
    }
}
