<?php

namespace Bss\Faqs\Model\ResourceModel\Faq;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Bss\Faqs\Model\Faq::class,
            \Bss\Faqs\Model\ResourceModel\Faq::class
        );
    }

    public function _initSelect()
    {
        parent::_initSelect();
        return $this->getSelect()
            ->joinLeft(
                ['secondTable' => $this->getTable('bss_faq_category')],
                'main_table.category_id = secondTable.category_id',
                ['category_title' => 'title']
            )
            ->joinLeft(
                ['thirdTable' => $this->getTable('admin_user')],
                'main_table.creator_id = thirdTable.user_id',
                ['creator' => 'username']
            );
    }
}
