<?php

namespace Bss\Faqs\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Bss\Faqs\Api\Data\FaqInterface;

interface FaqRepositoryInterface
{
    /**
     * @param int $faqId
     * @return \Bss\Faqs\Api\Data\FaqInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($faqId);

    /**
     * @param \Bss\Faqs\Api\Data\FaqInterface $faq
     * @return \Bss\Faqs\Api\Data\FaqInterface
     */
    public function save(FaqInterface $faq);

    /**
     * @param \Bss\Faqs\Api\Data\FaqInterface $faq
     * @return bool true on success
     */
    public function delete(FaqInterface $faq);

    /**
     * @param int $faqId
     * @return bool true on success
     */
    public function deleteById($faqId);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Bss\Faqs\Api\Data\FaqSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
