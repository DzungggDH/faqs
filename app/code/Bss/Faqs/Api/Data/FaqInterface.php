<?php

namespace Bss\Faqs\Api\Data;

interface FaqInterface
{
    /**
     * Constants for keys of data array.
     */
    const FAQ_ID = 'faq_id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const IS_ACTIVE = 'is_active';
    const CREATED_AT = 'created_at';
    const MODIFIED_AT = 'modified_at';

    /**
     * @return int
     */
    public function getId();

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id);

    /**
     * @return bool
     */
    public function isActive();

    /**
     * @param bool $isActive
     * @return $this
     */
    public function setIsActive($isActive);

    /**
     * @return string
     */
    public function getTitle();

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle($title);

    /**
     * @return string|null
     */
    public function getContent();

    /**
     * @param string $content
     * @return $this
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getModifiedAt();
}
