<?php

namespace Bss\Faqs\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface FaqSearchResultsInterface extends SearchResultsInterface
{
    /**
     * @return \Bss\Faqs\Api\Data\FaqInterface[]
     */
    public function getItems();

    /**
     * @param \Bss\Faqs\Api\Data\FaqInterface[] $items
     * @return void
     */
    public function setItems(array $items);
}
