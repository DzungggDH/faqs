<?php

namespace Bss\Faqs\Controller\Adminhtml\Category;

use Bss\Faqs\Model\FaqCategoryFactory;
use Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;

class MassDelete extends Action
{
    /**
     * @var \Bss\Faqs\Model\FaqCategoryFactory
     */
    private $faqCategoryFactory;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    private $filter;

    /**
     * @var \Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    private $resultRedirect;

    /**
     * @param Action\Context $context
     * @param FaqCategoryFactory $faqCategoryFactory
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        FaqCategoryFactory $faqCategoryFactory,
        Filter $filter,
        CollectionFactory $collectionFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $categoryIds = $this->getRequest()->getParams()['selected'];

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $deleted = 0;
        $err = 0;
        foreach ($categoryIds as $categoryId) {
            $deleteFaqCategory = $this->faqCategoryFactory->create()->load($categoryId);
            try {
                $deleteFaqCategory->delete();
                $deleted++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($deleted > 0) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deleted)
            );
        }

        if ($err > 0) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('faqs/category/index');
    }
}
