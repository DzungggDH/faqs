<?php

namespace Bss\Faqs\Controller\Adminhtml\Category;

use Bss\Faqs\Model\FaqCategoryFactory;
use Bss\Faqs\Model\ResourceModel\FaqCategory\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;

class Delete extends Action implements HttpGetActionInterface
{
    protected $faqCategoryFactory;

    protected $collectionFactory;

    public function __construct(
        Context $context,
        FaqCategoryFactory $faqCategoryFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Bss_Faqs::faq_category');
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('id', null);
        try {
            $faq = $this->faqCategoryFactory->create();
            $faq->load($id);
            if ($faq->getId()) {
                $faq->delete();
                $this->messageManager->addSuccessMessage(__('You deleted the record.'));
            } else {
                $this->messageManager->addErrorMessage(__('Record does not exist.'));
            }
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }
        return $resultRedirect->setPath('*/*');
    }
}
