<?php

namespace Bss\Faqs\Controller\Adminhtml\Category;

use Bss\Faqs\Model\FaqCategoryFactory;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var \Bss\Faqs\Model\FaqCategoryFactory
     */
    private $faqCategoryFactory;

    /**
     * @param Action\Context $context
     * @param FaqCategoryFactory $faqCategoryFactory
     */
    public function __construct(
        Action\Context $context,
        FaqCategoryFactory $faqCategoryFactory
    ) {
        parent::__construct($context);
        $this->faqCategoryFactory = $faqCategoryFactory;
    }

    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $id = !empty($postData['category_id']) ? $postData['category_id'] : null;
        $icon = !empty($postData['icon'][0]['name']) ? $postData['icon'][0]['name'] : null;

        $newData = [
            'title' => $postData['title'],
            'is_active' => $postData['is_active'],
            'icon' => $icon
        ];

        $faqCategory = $this->faqCategoryFactory->create();

        if ($id != null) {
            $faqCategory->load($id);
        }
        try {
            $faqCategory->addData($newData);
            $faqCategory->save();
            $this->messageManager->addSuccessMessage(__('You saved the FAQ Category.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['id' => $faqCategory->getId(), '_current' => true]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
