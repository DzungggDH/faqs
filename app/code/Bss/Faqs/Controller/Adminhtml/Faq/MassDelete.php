<?php

namespace Bss\Faqs\Controller\Adminhtml\Faq;

use Bss\Faqs\Model\FaqFactory;
use Bss\Faqs\Model\ResourceModel\Faq\CollectionFactory;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Backend\Model\View\Result\RedirectFactory;

class MassDelete extends Action
{
    /**
     * @var \Bss\Faqs\Model\FaqFactory
     */
    private $faqFactory;

    /**
     * @var \Bss\Faqs\Model\ResourceModel\Faq\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\RedirectFactory
     */
    private $resultRedirect;

    /**
     * @param Action\Context $context
     * @param FaqFactory $faqFactory
     * @param CollectionFactory $collectionFactory
     * @param RedirectFactory $redirectFactory
     */
    public function __construct(
        Action\Context $context,
        FaqFactory $faqFactory,
        CollectionFactory $collectionFactory,
        RedirectFactory $redirectFactory
    ) {
        parent::__construct($context);
        $this->faqFactory = $faqFactory;
        $this->collectionFactory = $collectionFactory;
        $this->resultRedirect = $redirectFactory;
    }

    public function execute()
    {
        $faqIds = $this->getRequest()->getParams()['selected'];

        $deleted = 0;
        $err = 0;
        foreach ($faqIds as $faqId) {
            $deleteFaq= $this->faqFactory->create()->load($faqId);
            try {
                $deleteFaq->delete();
                $deleted++;
            } catch (LocalizedException $exception) {
                $err++;
            }
        }

        if ($deleted > 0) {
            $this->messageManager->addSuccessMessage(
                __('A total of %1 record(s) have been deleted.', $deleted)
            );
        }

        if ($err > 0) {
            $this->messageManager->addErrorMessage(
                __(
                    'A total of %1 record(s) haven\'t been deleted. Please see server logs for more details.',
                    $err
                )
            );
        }
        return $this->resultRedirect->create()->setPath('faqs/faq/index');
    }
}
