<?php

namespace Bss\Faqs\Controller\Adminhtml\Faq;

use Bss\Faqs\Model\FaqFactory;
use Magento\Backend\App\Action;
use Magento\Authorization\Model\UserContextInterface;

class Save extends Action
{
    /**
     * @var \Bss\Faqs\Model\FaqFactory
     */
    private $faqFactory;

    /**
     * @var UserContextInterface
     */
    protected $userContext;

    /**
     * @param Action\Context $context
     * @param FaqFactory $faqFactory
     */
    public function __construct(
        Action\Context $context,
        FaqFactory $faqFactory,
        UserContextInterface $userContext
    ) {
        parent::__construct($context);
        $this->faqFactory = $faqFactory;
        $this->userContext = $userContext;
    }

    public function execute()
    {
        $postData = $this->getRequest()->getPostValue();
        $id = !empty($postData['faq_id']) ? $postData['faq_id'] : null;

        $newData = [
            'creator_id' => $this->userContext->getUserId(),
            'category_id' => $postData['category_id'],
            'title' => $postData['title'],
            'content' => $postData['content'],
            'is_active' => $postData['is_active']
        ];

        $faq = $this->faqFactory->create();

        if ($id != null) {
            $faq->load($id);
        }
        try {
            $faq->addData($newData);
            $faq->save();
            $this->messageManager->addSuccessMessage(__('You saved the FAQ Category.'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage()));
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        if ($this->getRequest()->getParam('back')) {
            return $resultRedirect->setPath('*/*/edit', ['id' => $faq->getId(), '_current' => true]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
