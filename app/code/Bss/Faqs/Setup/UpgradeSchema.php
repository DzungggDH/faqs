<?php

namespace Bss\Faqs\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;
use Exception;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '1.0.0', '<')) {
            $table = $setup->getConnection()
                ->newTable($setup->getTable('bss_faq_category'))
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Faq category ID'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_BOOLEAN,
                    null,
                    [
                        'nullable' => false,
                        'default' => true
                    ],
                    'Faq category status'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false,
                    ],
                    'Faq category title'
                )
                ->addColumn(
                    'icon',
                    Table::TYPE_TEXT,
                    255,
                    [
                        'nullable' => false
                    ],
                    'Faq category icon'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT
                    ],
                    'Created at'
                )
                ->addColumn(
                    'modified_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT_UPDATE
                    ],
                    'Modified at'
                )
                ->setComment("Faq category table");
            $setup->getConnection()->createTable($table);

            $table = $setup->getConnection()
                ->newTable($setup->getTable('bss_faq'))
                ->addColumn(
                    'faq_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'Faq ID'
                )
                ->addColumn(
                    'category_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Faq category'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'bss_faq',
                        'category_id',
                        'bss_faq_category',
                        'category_id'
                    ),
                    'category_id',
                    $setup->getTable('bss_faq_category'),
                    'category_id'
                )
                ->addColumn(
                    'creator_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'unsigned' => true,
                        'nullable' => false
                    ],
                    'Faq creator'
                )
                ->addForeignKey(
                    $setup->getFkName(
                        'bss_faq',
                        'creator_id',
                        'admin_user',
                        'user_id'
                    ),
                    'creator_id',
                    $setup->getTable('admin_user'),
                    'user_id'
                )
                ->addColumn(
                    'is_active',
                    Table::TYPE_BOOLEAN,
                    null,
                    [
                        'nullable' => false,
                        'default' => true
                    ],
                    'Faq status'
                )
                ->addColumn(
                    'title',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => false,
                        'default' => '',
                    ],
                    'Faq title'
                )
                ->addColumn(
                    'content',
                    Table::TYPE_TEXT,
                    null,
                    [
                        'nullable' => true,
                        'default' => 'No answer',
                    ],
                    'Faq answer'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT
                    ],
                    'Created at'
                )
                ->addColumn(
                    'modified_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    [
                        'nullable' => false,
                        'default' => Table::TIMESTAMP_INIT_UPDATE
                    ],
                    'Modified at'
                )
                ->setComment("Faq table");
            $setup->getConnection()->createTable($table);
        }
        $setup->endSetup();
    }
}
