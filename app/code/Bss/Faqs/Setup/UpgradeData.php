<?php

namespace Bss\Faqs\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Bss\Faqs\Model\FaqCategoryFactory;
use Bss\Faqs\Model\FaqFactory;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var FaqCategoryFactory
     */
    protected $faqCategoryFactory;

    /**
     * @var FaqFactory
     */
    protected $faqFactory;

    public function __construct(
        FaqCategoryFactory $faqCategoryFactory,
        FaqFactory $faqFactory
    ) {
        $this->faqCategoryFactory = $faqCategoryFactory;
        $this->faqFactory = $faqFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.0.0', '<')) {

            // add data to faq category table
            $categoryData = [
                [
                    'is_active' => true,
                    'title' => 'Easy',
                    'icon' => 'https://picsum.photos/100/100'
                ],
                [
                    'is_active' => true,
                    'title' => 'Medium',
                    'icon' => 'https://picsum.photos/100/100'
                ],
                [
                    'is_active' => true,
                    'title' => 'Hard',
                    'icon' => 'https://picsum.photos/100/100'
                ]
            ];

            foreach ($categoryData as $data) {
                $faqCategory = $this->faqCategoryFactory->create();
                $faqCategory->addData($data)->save();
            }

            // add data to faq table
            $faqData = [
                [
                    'category_id' => 1,
                    'creator_id' => 1,
                    'is_active' => true,
                    'title' => 'How to say Hello World?',
                    'content' => 'Say hello A then hello B'
                ],
                [
                    'category_id' => 2,
                    'creator_id' => 1,
                    'is_active' => true,
                    'title' => 'Question 2?',
                    'content' => 'Say hello B then hello C'
                ],
                [
                    'category_id' => 3,
                    'creator_id' => 1,
                    'is_active' => true,
                    'title' => 'Question 3?',
                    'content' => 'Say hello C then hello D'
                ]
            ];
            foreach ($faqData as $data) {
                $faq = $this->faqFactory->create();
                $faq->addData($data)->save();
            }
        }
    }
}
